var sys = require('sys');
var fs = require('fs');
var util = require('util');

var port = 88;
var path = '/home/httpd/vhosts/the-grotto.com/jiveHome/attachments/';

var express = require('express')

var app = express();

app.all('/stream/:fileid', function(req, res){
  //sys.puts(util.inspect(req.headers, showHidden=false, depth=0));
  
  var file = path+req.params.fileid + '.bin';
  var stat = fs.statSync(file);
  if (!stat.isFile()) return;

  var start = 0;
  var end = 0;
  var range = req.header('Range');
  if (range != null) {
    start = parseInt(range.slice(range.indexOf('bytes=')+6,
      range.indexOf('-')));
    end = parseInt(range.slice(range.indexOf('-')+1,
      range.length));
  }
  if (isNaN(end) || end == 0) end = stat.size-1;

  if (start > end) return;

  //sys.puts('Browser requested bytes from ' + start + ' to ' +   end + ' of file ' + file);

  var date = new Date();

if(req.method == 'HEAD') {  
  res.header('Connection','close');
  res.header('Content-Range','bytes '+start+'-'+end+'/'+stat.size);
  res.header('Transfer-Encoding','chunked');
  res.header('Content-Length',end-start);
  res.header('Accept-Ranges','bytes');
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Methods','GET,POST,PUT,RANGE,HEAD,OPTIONS');
  res.header('Access-Control-Expose-Headers','Content-Length, Content-Range');
  res.header('Access-Control-Allow-Headers','X-Requested-With, Range, Origin, Content-Type, Accept');
  res.send(206,"Success");
} else {

  res.writeHead(206, { // NOTE: a partial http response
    // 'Date':date.toUTCString(),
    'Connection':'close',
    // 'Cache-Control':'private',
    // 'Content-Type':'video/webm',
    // 'Content-Length':end - start,
    'Content-Range':'bytes '+start+'-'+end+'/'+stat.size,
    // 'Accept-Ranges':'bytes',
    // 'Server':'CustomStreamer/0.0.1',
    'Transfer-Encoding':'chunked',
    'Access-Control-Allow-Origin':'*',
    'Access-Control-Allow-Methods':'GET,POST,PUT,RANGE,HEAD,OPTIONS',
    'Access-Control-Expose-Headers':'Content-Length, Accept-Ranges, Content-Range',
    'Access-Control-Allow-Headers':'X-Requested-With, Range, Origin, Content-Type, Accept'
    });
 
	 var stream = fs.createReadStream(file,
    	{ flags: 'r', start: start, end: end});
	  stream.pipe(res);
}
});

app.listen(port);

process.on('uncaughtException', function(err) {
  sys.puts(err);
});