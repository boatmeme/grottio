				 
$(document).ready(function() {
				 var player;
				 var duration = 0;
				 var current;
				 var volume = 1.0;
				 var audio = new Audio();
				 
				 var infoDiv = $("<div class='info'><span class='loader'></span><span class='buffer'></span><span class='progress'></span><span class='time'></span></div>");
				 var volumeDiv = $("<div class='volume'><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div><div class='vol'></div></div>");
				 var streamUrl = "http://the-grotto.com:88/stream/";
				 
				 
				 function millisToTimeString(ms) {
					var x = ms / 1000;
					var seconds = Math.floor(x % 60)
					seconds = seconds < 10 ? "0" + seconds : seconds
					x /= 60
					var minutes = Math.floor(x % 60)
					minutes = minutes < 10 ? "0" + minutes : minutes 
					return minutes + ":" + seconds;
				 }
				 
				 function secsToTimeString(time) {
					var x = time;
					var seconds = Math.floor(x % 60)
					seconds = seconds < 10 ? "0" + seconds : seconds
					x /= 60
					var minutes = Math.floor(x % 60)
					minutes = minutes < 10 ? "0" + minutes : minutes 
					return minutes + ":" + seconds;
				 }
				 
				 function start() {
					cleanup();
					var mediaUrl = streamUrl + this.id;
					
					current = $(this);
					current.append(infoDiv.clone());
					current.append(volumeDiv.clone());
					current.unbind('click',start);
					current.bind('click',play);
					
					if(this.href.indexOf("mp3") > -1 && audio.canPlayType('audio/mp3')) {
						player = audio;
						player.src = mediaUrl;
						player.addEventListener("progress",function(evt) {
							if(this.buffered!==undefined && this.buffered.length != 0) {
								var loaded = this.buffered.end(0);
								var percent = (loaded/this.duration) * 100;
								$(".buffer",current).css('width',percent + "%");
							}
						});
						
						player.addEventListener("timeupdate",function(evt) {
							var length = parseInt(this.duration);
							var currentTime = parseInt(this.currentTime);
							var percent = (currentTime/length) * 100;
							$(".progress",current).css('width',percent + "%");
							if(length>0) {
								$(".time",current).html(secsToTimeString(currentTime) + " / " + secsToTimeString(length));
								} else {
								$(".time",current).html(secsToTimeString(currentTime));
							}
						});
						
						$(".info",current).bind('click',function(e){
						
							var x;
							if (e.pageX || e.pageY) { 
							  x = e.pageX;
							}
							else { 
							  x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
							} 
							x -= this.offsetLeft + 6;
							percent = x / $(this).width();
							player.currentTime = player.duration * percent;
							return false;
						});			
					} else {
						player = Player.fromURL(mediaUrl);
						player.on("duration",function(dur) {
							duration = dur;
						});
						player.on("buffer",function(percent) {
							$(".buffer",current).css('width',percent + "%");
						});
						player.on("progress",function(currentTime) {
							var percent = (currentTime/duration) * 100;
							$(".progress",current).css('width',percent + "%");
							if(duration>0) {
								$(".time",current).html(millisToTimeString(currentTime) + " / " + millisToTimeString(duration));
								} else {
								$(".time",current).html(millisToTimeString(currentTime));
							}
							
						});
						player.on("end", function(currentTime) {
							next();
						});				
					}
					setVolume(volume);
					$(".volume",current).bind('click',function(){return false;});
					$(".vol", current).bind('click', onVolumeChange);
					$(this).click();
					return false;
				 }
				 
				 function getVolumeByEl(el) {
					var vols = $(".vol",current);
					var index = vols.index(el);
					return ((index+1)/vols.length);
				 }
				 
				 function initVolume(volume) {
					var vols = $(".vol", current);
					vols.removeClass("full");
					var max = Math.floor(vols.length * (volume/1));
					for(var i = 0; i<max; i++) {
						$(vols[i]).addClass("full");
					}
				 }
				 
				 function onVolumeChange() {
					setVolume(getVolumeByEl($(this)));
					return false;
				 }
				 
				 function setVolume(vol) {				
					if(player!==undefined) {
						volume = vol;
						player.volume = player instanceof HTMLAudioElement ? volume : volume*100
						initVolume(volume);
					}
				 }
				 
				 function next() {
					if(current!==undefined) {
						var audio = $(".audio");
						var index = audio.index(current);
						if(index < audio.length-1) {
							audio.get(index+1).click();
						} else {
							cleanup();
						}
					}
				 }
				 
				 function cleanup() {
					if(player!==undefined) {
						console.log(player instanceof HTMLAudioElement);
						if(player instanceof HTMLAudioElement) {
							player.pause();
							player.progress = null;
							player.timeupdate = null;
						} else {
							player.stop();
							$(player).remove();
						}
						$(player).unbind();
						delete player;
					} 
					
					if(current!==undefined) {
						$(".time",current).remove();
						$(".info",current).remove();
						$(".volume",current).remove();
						current.removeClass("player_paused");
						current.removeClass("player_playing");
						current.unbind('click');
						current.bind('click',start);
					}
					duration = 0;
				 }
				 
				 
				 function play() {
					$(this).removeClass("player_paused");
					$(this).addClass("player_playing");

					player.play();
					$(this).unbind('click',play);
					$(this).bind('click',pause);
					return false;
				 }
				 
				 function pause() {
					$(this).removeClass("player_playing");
					$(this).addClass("player_paused");
					player.pause();
					$(this).unbind('click',pause);
					$(this).bind('click',play);
					return false;
				 }
				 
				var audioTagSupport = !!(document.createElement('audio').canPlayType);
				if(audioTagSupport) {
					$(".audio").each(function() {
						if(this.href.indexOf("mp3") > -1 || this.href.indexOf("flac") > -1) {
							//var img = $("<img src='/talk/images/attachment.gif'/></a>");
							//var href = this.href;
							//img.bind('click',function() {window.location.href = href});
							//$(this).append(img);
							$(this).addClass("player_link");
							$(this).bind('click',start);
						}
					});
				}
});